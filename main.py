import atexit  # модуль для автосохранения
import json  # модуль для работы с файлами формата json
import re  # модуль для валидации вводимых данных


class TelephoneDirectory:  # основной класс с методами для справочника
    def __init__(self):
        self.contacts = []
        atexit.register(self.save_file, "telephone_directory.json")  # автосохранение файла

    @staticmethod
    def validate_name(name):
        return bool(re.match(r'^[А-Яа-яA-Za-z\s]+$', name))  # валидация для ФИО

    @staticmethod
    def validate_organization(organization):
        return bool(re.match(r'^[А-Яа-яA-Za-z0-9\s]+$', organization))  # валидация для поля "организация"

    @staticmethod
    def validate_telephone_number(telephone_number):
        return bool(re.match(r'^8\d{10}$', telephone_number))  # валидация для телефонных номеров

    def add_contact(self, last_name, first_name, surname, organization, job_telephone,
                    personal_telephone):  # метод добавления контакта в справочник
        if self.validate_name(last_name) and \
                self.validate_name(first_name) and \
                self.validate_name(surname) and \
                self.validate_organization(organization) and \
                self.validate_telephone_number(job_telephone) and \
                self.validate_telephone_number(personal_telephone):
            contact_id = len(self.contacts) + 1  # выдает id контакту
            self.contacts.append({  # добавляет контакт к справочнику
                "id": contact_id,
                "last_name": last_name,
                "first_name": first_name,
                "surname": surname,
                "organization": organization,
                "job_telephone": job_telephone,
                "personal_telephone": personal_telephone
            })
            print("вы добавили контакт к справочнику.")
        else:
            print("вы ввели неправильные данные.")

    def display_contacts(self, contacts=None):  # метод для отображения справочника
        if not contacts:
            contacts = self.contacts
        if not contacts:
            print("контакты не найдены.")
            return
        print("контакты:")
        for contact in contacts:
            print(f"ID: {contact['id']}")
            print(f"фамилия: {contact['last_name']}")
            print(f"имя: {contact['first_name']}")
            print(f"отчество: {contact['surname']}")
            print(f"организация: {contact['organization']}")
            print(f"рабочий телефон: {contact['job_telephone']}")
            print(f"личный телефон: {contact['personal_telephone']}")
            print()
        print(f"всего контактов в справочнике: {len(contacts)}")

    def get_contact_by_id(self, contact_id):  # метод для получения информации о контакте по его id
        for contact in self.contacts:
            if contact['id'] == contact_id:
                return contact
        return None

    def count_contacts(self):  # подсчет всех контактов в справочнике (считается длина списка self.contacts)
        return len(self.contacts)

    def search_contacts(self, **kwargs):  # метод для поиска контактов
        results = []  # строка для рез-ов
        for contact in self.contacts:
            match = True  # переменная для проверки сов-ния полей
            for key, value in kwargs.items():
                if contact.get(key,
                               "").lower() != value.lower():  # поиск зн-ния по словарю "contact", lower нужен на
                    # случай, если пользователь не учел регистр вводимых символов
                    match = False
                    break
            if match:
                results.append(contact)
        return results

    def edit_contact(self, contact_id):  # метод для редактирования контакта в справочнике
        contact = self.get_contact_by_id(contact_id)
        if contact:
            print(f"Редактирование контакта с ID {contact_id}:")
            for match in ['last_name', 'first_name', 'middle_name', 'organization', 'work_phone',
                          'personal_phone']:  # поле для редактирования
                new_value = input(f"Введите новое значение для {contact.get(match, '').replace('_', ' ').title()}: ")
                if match == 'id':
                    continue
                if match in ['work_phone', 'personal_phone'] and not self.validate_telephone_number(new_value):
                    print("Некорректный номер телефона.")
                    return
                elif match in ['last_name', 'first_name', 'middle_name'] and not self.validate_name(new_value):
                    print("Некорректное ФИО.")
                    return
                elif match == 'organization' and not self.validate_organization(new_value):
                    print("Некорректное название организации.")
                    return
                contact[match] = new_value  # смена атрибутов класса на ключи словаря
            print("Контакт успешно изменен!")
        else:
            print("Контакт с указанным ID не найден.")

    def save_file(self, filename):  # метод для сохранения контактов в справочнике
        with open(filename, "w") as file:  # 'w' - write
            json.dump(self.contacts, file)
        print(f"контакты сохранены в файл: {filename}")

    def load_from_file(self, filename):  # метод для загрузки контактов из справочника
        try:
            with open(filename, "r") as file:  # 'r' - read
                self.contacts = json.load(file)
            print(f"контакты загружены из файла: {filename}")
        except FileNotFoundError:  # ошибки
            print("файл телефонной книги не найден. Вы начнете работу с пустым справочником.")
        except Exception as e:  # ошибки
            print(f"ошибка загрузки контактов из файла: {str(e)}")


def testovoe_1():  # основная функция
    telephone_directory = TelephoneDirectory()  # экземпляр класса
    options_mapping = {
        # словарь, связывает русские названия с аттрибутами в классе (создавался для того, чтобы поиск работал
        # корректно при вводе критерия поиска)
        "фамилия": "last_name",
        "имя": "first_name",
        "отчество": "surname",
        "организация": "organization",
        "рабочий телефон": "job_telephone",
        "личный телефон": "personal_telephone"
    }

    telephone_directory.load_from_file("telephone_directory.json")  # загрузка справочника

    print('добро пожаловать в телефонный справочник. для получения справки введите "!справка"')

    while True:

        menu = input(
            'введите команду (для справки введите "!справка"): ')  # меню для удобного ориентирования пользователя

        if menu == "!справка":
            print('введите "!добавить контакт", чтобы добавить к справочнику новый контакт')
            print('введите "!показать все контакты", чтобы показать весь список контактов')
            print('введите "!поиск контактов", чтобы провести поиск по справочнику')
            print('введите "!изменить контакт" (требуется ID контакта), чтобы изменить существующий контакт')
            print('введите "!сохранить контакты", чтобы сохранить существующие контакты')
            print('введите "!кол-во контактов", чтобы увидеть общее число контактов')
            print('введите "!выход", чтобы завершить работу кода')

        elif menu == "!добавить контакт":
            last_name = input('фамилия: ')
            first_name = input('имя: ')
            surname = input('отчество: ')
            organization = input('организация: ')
            job_telephone = input('рабочий телефон: ')
            personal_telephone = input('личный телефон: ')
            telephone_directory.add_contact(last_name, first_name, surname, organization, job_telephone,
                                            personal_telephone)  # вызов метода для создания контакта

        elif menu == "!показать все контакты":
            telephone_directory.display_contacts()  # вызов метода для отображения контактов

        elif menu == "!поиск контактов":
            search_options = input(
                'введите поле, по которому хотите провести поиск (фамилия, имя, отчество, '
                'организация, рабочий телефон, личный телефон): ')
            search_options = options_mapping.get(
                search_options.lower())  # преобразование русских критериев к атрибутам класса
            if search_options:
                search_value = input('введите значение для поиска: ')
                search_results = telephone_directory.search_contacts(
                    **{search_options: search_value})  # вызывается метода поиска, передавая ему критерий и содержание
                telephone_directory.display_contacts(
                    search_results)  # вызов метода для отображения контактов (да, для оптимизации это тот же метод,
                # что и для отображения всех контактов, хотя это наверное было понятно и выше)
            else:
                print("вы ввели неправильное поле.")

        elif menu == "!изменить контакт":
            contact_id = int(input('введите ID контакта для редактирования: '))
            telephone_directory.edit_contact(contact_id)  # вызов метода для редактирования с передачей id

        elif menu == "!сохранить контакты":
            telephone_directory.save_file("telephone_directory.json")  # вызов метода для сохранения

        elif menu == "!кол-во контактов":
            print(f"общее кол-во контактов: {telephone_directory.count_contacts()}")

        elif menu == "!выход":
            print("завершение работы кода")
            break

        else:
            print('вы ввели неправильную команду. введите "!справка" для помощи в меню')


if __name__ == "__main__":  # вызов основной функции, если скрипт запущен напрямую
    testovoe_1()
